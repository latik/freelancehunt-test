.DEFAULT_GOAL := help

install: ## init project
	test -f .env || cp -n .env.example .env
	composer install
	php bin/doctrine.php orm:schema-tool:update --force --complete

fetch: ## refresh db projects
	php bin/cron.php

start: ## start project
	php -S 0.0.0.0:8800 -t public

help: ## Help message
	@echo "Please choose a task:"
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
