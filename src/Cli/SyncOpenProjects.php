<?php

declare(strict_types=1);

namespace App\Cli;

use App\ExternalServices\Freelancehunt\Factory;
use App\ExternalServices\Freelancehunt\Models\ProjectEmployer;
use App\ExternalServices\Freelancehunt\Models\ProjectSkill;
use App\Models\Employer;
use App\Models\Project\Project;
use App\Models\Project\ProjectAttributes;
use App\Models\Project\ProjectBudget;
use App\Models\Skill;
use App\Persistence\EntityManagerFactory;
use Doctrine\ORM\EntityManager;

final class SyncOpenProjects
{
    private EntityManager $entityManager;

    public function __construct()
    {
        $this->entityManager = EntityManagerFactory::create();
    }

    public function __invoke(): void
    {
        $factory = new Factory();
        $apiClient = $factory::create();

        $projectsDto = $apiClient->getOpenProjects();

        foreach ($projectsDto as $projectDto) {
            $project = $this->entityManager->find(Project::class, $projectDto->id);
            if ($project) {
                continue;
            }

            $attributes = $projectDto->attributes;

            $project = new Project();
            $project->id = $projectDto->id;

            $project->budget = new ProjectBudget($attributes->budget->amount ?? 0, $attributes->budget->currency ?? ProjectBudget::CURRENCY_UAH);

            $project->attributes = new ProjectAttributes();
            $project->attributes->name = $attributes->name ?? '';
            $project->attributes->url = $projectDto->links->self->web ?? '';

            foreach ($attributes->skills as $skillDto) {
                $skill = $this->getSkill($skillDto);
                $project->addSkill($skill);
            }

            $employer = $this->getEmployer($attributes->employer);
            $project->employer = $employer;

            $this->entityManager->persist($project);
        }

        $this->entityManager->flush();
    }

    private function getEmployer(?ProjectEmployer $employerDto): ?Employer
    {
        $id = $employerDto->id ?? null;

        if ($id === null) {
            return null;
        }

        $employer = $this->entityManager->find(Employer::class, $id);
        if ($employer) {
            return $employer;
        }

        $employer = new Employer();
        $employer->id = $id;
        $employer->login = $employerDto->login ?? '';
        $employer->name = trim(sprintf('%s %s', $employerDto->first_name ?? '', $employerDto->last_name ?? ''));

        $this->entityManager->persist($employer);

        return $employer;
    }

    private function getSkill(ProjectSkill $skillDto): Skill
    {
        $id = $skillDto->id ?? null;

        $skill = $this->entityManager->find(Skill::class, $id);
        if ($skill) {
            return $skill;
        }

        $skill = new Skill();
        $skill->id = $skillDto->id;
        $skill->name = $skillDto->name;

        $this->entityManager->persist($skill);

        return $skill;
    }
}
