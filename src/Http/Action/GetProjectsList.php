<?php

declare(strict_types=1);

namespace App\Http\Action;

use App\Persistence\EntityManagerFactory;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

final class GetProjectsList
{
    private EntityManager $entityManager;

    public function __construct()
    {
        $this->entityManager = EntityManagerFactory::create();
    }

    public function __invoke(Request $request)
    {
        $skillsIds = $request->get('skillsIds');
        $page = $request->get('page', 1);
        $perPage = $request->get('perPage', 1000);

        $firstResult = ($page - 1) * $perPage;

        $dql = "
            SELECT p, s, e
            FROM App\Models\Project\Project p
            LEFT JOIN p.skills s
            LEFT JOIN p.employer e
        ";

        if (!empty($skillsIds)) {
            $dql .= 'WHERE s.id IN (:skillsIds)';
        }

        $query = $this->entityManager->createQuery($dql)
            ->setFirstResult($firstResult)
            ->setMaxResults($perPage);

        if (!empty($skillsIds)) {
            $skillsIdsArray = explode(',', $skillsIds);
            $query->setParameters(['skillsIds' => $skillsIdsArray]);
        }

        $projectsInfo = $query->getArrayResult();

        $result = [];
        foreach ($projectsInfo as $info) {
            $result[] = [
                'id' => $info['id'],
                'name' => $info['attributes.name'],
                'url' => $info['attributes.url'],
                'employer' => $info['employer'],
                'skills' => $info['skills'],
                'budget' => [
                    'amount' => $info['budget.amount'],
                    'currency' => $info['budget.currency'],
                ],
            ];
        }

        echo json_encode($result);
    }
}
