<?php

declare(strict_types=1);

namespace App\Http\Action;

final class GetDashboardPage
{
    public function __invoke()
    {
        $template = \file_get_contents(__DIR__ . '/../../../templates/home.html');

        echo $template;
    }
}
