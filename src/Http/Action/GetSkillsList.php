<?php

declare(strict_types=1);

namespace App\Http\Action;

use App\Persistence\EntityManagerFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Pagination\Paginator;

final class GetSkillsList
{
    private EntityManager $entityManager;

    public function __construct()
    {
        $this->entityManager = EntityManagerFactory::create();
    }

    public function __invoke()
    {
        $page = 1;
        $perPage = 50;

        $firstResult = ($page - 1) * $perPage;

        $dql = "
            SELECT s
            FROM App\Models\Skill s
        ";

        $query = $this->entityManager->createQuery($dql)
            ->setFirstResult($firstResult)
            ->setMaxResults($perPage);

        $skillsInfo = $query->getArrayResult();

        echo json_encode($skillsInfo);
    }
}
