<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt;

use App\ExternalServices\Freelancehunt\Models\Project;
use GuzzleHttp\Client as HttpClient;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;

final class Client
{
    private string $apiToken;
    private ClientInterface $httpClient;
    private \JsonMapper $mapper;

    public function __construct(string $apiToken, ?ClientInterface $httpClient = null)
    {
        $this->httpClient = $httpClient ?: new HttpClient();
        $this->apiToken = $apiToken;
        $this->mapper = new \JsonMapper();
    }

    private function request(string $method, string $uri = '', array $options = []): ResponseInterface
    {
        $requestOptions = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->apiToken,
                    'Accept' => 'application/json',
                ],
            ] + $options;

        return $this->httpClient->request(method: $method, uri: $uri, options: $requestOptions);
    }

    private function getContents(ResponseInterface $response): \stdClass
    {
        return json_decode($response->getBody()->getContents());
    }

    /**
     * @return array<Project>
     */
    public function getOpenProjects(): array
    {
        $pageUri = 'projects';

        do {
            $response = $this->getOpenProjectsPage($pageUri);

            $contents = $this->getContents($response);

            $projectsData[] = $this->mapper->mapArray($contents->data ?? '', [], Project::class);

            $pageUri = $contents->links->next ?? '';

        } while ($pageUri);

        return array_merge([], ...$projectsData);
    }

    private function getOpenProjectsPage(string $pageUri): ResponseInterface
    {
        return $this->request('GET', uri: $pageUri); // TODO: error handler and retry
    }
}
