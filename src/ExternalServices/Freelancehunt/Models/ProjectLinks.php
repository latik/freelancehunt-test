<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class ProjectLinks
{
    public ProjectSelfLink $self;
}
