<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class ProjectAttributes
{
    public string $name;
    public ?ProjectBudget $budget;
    public ?ProjectEmployer $employer;

    /**
     * @var ProjectSkill[]
     */
    public array $skills;
}
