<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class Project
{
    public int $id;

    public ProjectAttributes $attributes;

    public ProjectLinks $links;
}
