<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class ProjectSelfLink
{
    public ?string $api = null;
    public ?string $web = null;
}
