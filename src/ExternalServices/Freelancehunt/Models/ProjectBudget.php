<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class ProjectBudget
{
    public int $amount;
    public string $currency;
}
