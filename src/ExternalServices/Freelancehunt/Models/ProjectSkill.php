<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class ProjectSkill
{
    public int $id;
    public string $name;
}
