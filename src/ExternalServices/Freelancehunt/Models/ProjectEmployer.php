<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt\Models;

final class ProjectEmployer
{
    public int $id;
    public ?string $login = null;
    public ?string $first_name = null;
    public ?string $last_name = null;
}
