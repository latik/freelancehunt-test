<?php

declare(strict_types=1);

namespace App\ExternalServices\Freelancehunt;

use GuzzleHttp\Client as HttpClient;

final class Factory
{
    public static function create(): Client
    {
        $baseUrl = \getenv('FREELANCEHUNT_API_URI');
        $apiKey = \getenv('FREELANCEHUNT_API_TOKEN');

        $config = [
            'base_uri' => $baseUrl,
        ];

        $httpClient = new HttpClient($config);

        return new Client($apiKey, $httpClient);
    }
}
