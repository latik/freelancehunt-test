<?php

declare(strict_types=1);

namespace App\Models;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Skill
{
    #[ORM\Column, ORM\Id]
    public int $id;

    #[ORM\Column]
    public string $name;
}
