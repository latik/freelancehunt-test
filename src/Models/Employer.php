<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Project\Project;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Employer
{
    #[ORM\Column, ORM\Id]
    public int $id;

    #[ORM\Column]
    public string $login;

    #[ORM\Column]
    public string $name;

    #[ORM\OneToMany(mappedBy: 'employer', targetEntity: Project::class)]
    public Collection $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }
}
