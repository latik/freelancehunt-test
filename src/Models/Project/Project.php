<?php

declare(strict_types=1);

namespace App\Models\Project;

use App\Models\Employer;
use App\Models\Skill;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class Project
{
    #[ORM\Column, ORM\Id]
    public int $id;

    #[ORM\Embedded]
    public ProjectAttributes $attributes;

    #[ORM\Embedded]
    public ?ProjectBudget $budget;

    #[ORM\ManyToOne]
    public ?Employer $employer;

    #[ORM\ManyToMany(targetEntity: Skill::class)]
    public Collection $skills;

    public function __construct()
    {
        $this->skills = new ArrayCollection();
    }

    public function addSkill(Skill $skill): void
    {
        $this->skills->add($skill);
    }
}
