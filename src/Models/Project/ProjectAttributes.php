<?php

declare(strict_types=1);

namespace App\Models\Project;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class ProjectAttributes
{
    #[ORM\Column]
    public string $name;

    #[ORM\Column]
    public string $url;
}
