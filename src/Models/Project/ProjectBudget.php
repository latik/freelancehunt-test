<?php

declare(strict_types=1);

namespace App\Models\Project;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Embeddable]
final class ProjectBudget
{
    public const CURRENCY_UAH = 'UAH';

    #[ORM\Column]
    public int $amount;

    #[ORM\Column]
    public string $currency;

    public function __construct(int $amount, string $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }
}
