<?php

use App\Http\Action\GetDashboardPage;
use App\Http\Action\GetProjectsList;
use App\Http\Action\GetSkillsList;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use Symfony\Component\HttpFoundation\Request;

use function FastRoute\simpleDispatcher;

require __DIR__ . '/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(dirname(__DIR__));
$dotenv->load();

$dispatcher = simpleDispatcher(
    function (RouteCollector $router) {
        $router->addRoute('GET', '/', GetDashboardPage::class);
        $router->addRoute('GET', '/projects', GetProjectsList::class);
        $router->addRoute('GET', '/skills', GetSkillsList::class);
    }
);

$request = Request::createFromGlobals();

$routeInfo = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());

switch ($routeInfo[0]) {
    case Dispatcher::FOUND:
        $handler = $routeInfo[1] ?? '';
        $arguments = $routeInfo[2] ?? [];

        (new $handler)($request, ...$arguments);
        break;
    default:
    case Dispatcher::NOT_FOUND:
        http_response_code(404);
        break;
}
