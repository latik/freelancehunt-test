#!/usr/bin/env php
<?php

use App\Cli\SyncOpenProjects;

require __DIR__.'/../vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(dirname(__DIR__));
$dotenv->load();

(new SyncOpenProjects())();
