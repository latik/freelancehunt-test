#!/usr/bin/env php
<?php

use App\Persistence\EntityManagerFactory;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Console\EntityManagerProvider\SingleManagerProvider;

require __DIR__ . '/../vendor/autoload.php';

$entityManager = EntityManagerFactory::create();

ConsoleRunner::run(new SingleManagerProvider($entityManager));
