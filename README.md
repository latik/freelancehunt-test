Реализация тестового задания: https://github.com/freelancehunt/code-test

Сначала установить:

``make install``

Потом скачать/обновить базу проектов:

``make fetch``

Для запуска web сервера выполните команду:

``make start``

Смотреть:

http://127.0.0.1:8800
